from django.test import TestCase, Client
from .views import login, signup, logout, home
from django.contrib.auth.models import User

# Create your tests here.
class TestStory9(TestCase):
	def test_apakah_home_ada(self):
		response = Client().get('/')
		self.assertEquals(response.status_code, 200)
	def test_apakah_home_ada_htmlnya(self):
		response = Client().get('/')
		self.assertTemplateUsed(response, 'main/home.html')
	def test_apakah_login_ada(self):
		response = Client().get('/login/')
		self.assertEquals(response.status_code, 200)
	def test_apakah_login_ada_htmlnya(self):
		response = Client().get('/login/')
		self.assertTemplateUsed(response, 'main/login.html')
	def test_apakah_signup_ada(self):
		response = Client().get('/signup/')
		self.assertEquals(response.status_code, 200)
	def test_apakah_signup_ada_htmlnya(self):
		response = Client().get('/signup/')
		self.assertTemplateUsed(response, 'main/signup.html')
	def test_apakah_logout_ada(self):
		response = Client().get('/logout/')
		self.assertEquals(response.status_code, 302)
	def test_create_model(self):
		User.objects.create_user(username="admin", email="admin@gmail.com", password="admin")
		self.assertEqual(User.objects.all().count(), 1)
