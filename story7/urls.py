from django.urls import path

from . import views

app_name = 'main'

urlpatterns = [
    path('story7/', views.story7, name='story7'),
]
