$(document).ready(function() {
	$.ajaxSetup({ cache: false });

	$('#search').keyup(function(event){
		$('#list_result').html('');
		$('#result').html('');
		var searchField = $('#search').val();

		if (event.keyCode != 13) {
		  $.get("https://www.googleapis.com/books/v1/volumes?q="+searchField, function(data){
				$.each(data.items, function(key, value){
					var x = value.volumeInfo;
	    		$('#list_result').append('<li class="list-group-item">'+x.title+'</li>');
		  	});
			});
		}  else {
			console.log("search " + searchField);

			$.get("https://www.googleapis.com/books/v1/volumes?q="+searchField, function(data){
				$('#result').append(`
					<thead class="align-middle text-center font-weight-bold">
						<tr>
							<th> Sampul Buku </th>
							<th> Judul </th>
							<th> Penulis </th>
						</tr>
					</thead>
				`);
				
				$.each(data.items, function(key, value){
					var x = value.volumeInfo;
					var book = {
						cover: x.imageLinks.smallThumbnail,
						title: x.title,
						author: x.authors[0]
					}

					$.post('/create-book/', book, function(data){
						$('#result').append(`
							<tr>
								<td><img src=` + book.cover + ` width="120px"></td>
								<td>` + book.title + `</td>
								<td>` + book.author + `</td>
							</tr>
						`);
					});
				});
			});
		}
	});
});
