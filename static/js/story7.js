(function($) {
    
  var allPanels = $('.accordion .accordion-content dd').hide();
    
  $('.accordion .accordion-content dt a').click(function() {
      $this = $(this);
      $target =  $this.parent().next();
      
      if($target.hasClass('active')){
        $target.removeClass('active').slideUp(); 
      }else{
        allPanels.removeClass('active').slideUp();
        $target.addClass('active').slideDown();
      }
      
    return false;
  });

  $('.up').click(function() {
      var item = $(this).parents('div.accordion-content');
      item.insertBefore(item.prev());
  });

  $('.down').click(function() {
    var item = $(this).parents('div.accordion-content');
      item.insertAfter(item.next());
  });

})(jQuery);