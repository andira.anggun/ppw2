from django.urls import path

from . import views

urlpatterns = [
   path('story8/', views.story8, name='story8'),
   path('create-book/', views.create_book, name='create_book'),
]
