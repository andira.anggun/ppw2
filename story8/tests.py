from django.test import TestCase, Client
from .views import story8, create_book

class TestStory8(TestCase):
    def test_apakah_story8_ada(self):
        response = Client().get('/story8/')
        self.assertEquals(response.status_code, 200)

    def test_apakah_story8_ada_htmlnya(self):
        response = Client().get('/story8/')
        self.assertTemplateUsed(response, 'main/story8.html')

class TestCreateBook(TestCase):
	def test_create_book_ada(self):
		response = Client().get('/create-book/')
		self.assertEqual(response.status_code, 403)

