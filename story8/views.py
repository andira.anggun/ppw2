from django.shortcuts import render
from django.forms.models import model_to_dict
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse, HttpResponseForbidden


def story8(request):
    return render(request, 'main/story8.html')

@csrf_exempt
def create_book(request) :
	if(request.is_ajax()) :
		data = {}
		return JsonResponse(data)
	else :
		return HttpResponseForbidden()

